package myproject.automation.homework2.tests;

import myproject.automation.homework2.BaseScript;
import myproject.automation.homework2.pages.CategoriesPage;
import myproject.automation.homework2.pages.LoginPage;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CreateCategoryTest extends BaseScript {
    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = getConfiguredDriver();
        WebDriverWait wait = createWaitDriver(driver);
        String categoryName = "TestCategoryName";

        logIntoSystem(driver);

        CategoriesPage categoriesPage = new CategoriesPage(driver, wait);
        categoriesPage.openCategoriesPage();
        categoriesPage.waitUntilTitleWillBeVisible();
        categoriesPage.clickOnAddCategoryBtn();
        categoriesPage.fillCategoryName(categoryName);
        categoriesPage.clickSaveBtn();
        categoriesPage.waitUntilSuccessMessageWillBeVisible();
        categoriesPage.clickBackToCategoryListBtn();
        categoriesPage.sortByNameasc();

        Assert.assertTrue(categoriesPage.validatesIfCategoryIsAddedIntoTable(categoryName));

        quitDriver(driver);
    }

    private static void logIntoSystem(WebDriver driver){
        String email = "webinar.test@gmail.com";
        String password = "Xcg7299bnSmMuRLp9ITw";

        LoginPage loginPage = new LoginPage(driver);
        loginPage.goToLoginPage();
        loginPage.fillEmailInput(email);
        loginPage.fillPasswordInput(password);
        loginPage.clickLoginBtn();
    }
}
