package myproject.automation.homework2.pages;

import myproject.automation.homework2.Helper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class CategoriesPage extends Helper{
    WebDriver driver;
    WebDriverWait wait;

    private By catalogMenuItem = By.xpath("//span[contains(text(), 'Каталог')]//ancestor::a");
    private By categoriesMenuItem = By.xpath("//a[contains(text(), 'категории')]");
    private By title = By.xpath("//h2[contains(@class,'title')]");
    private By addCategoryBtn = By.cssSelector("#page-header-desc-category-new_category .process-icon-new");
    private By categoryNameInput = By.id("name_1");
    private By saveBtn = By.id("category_form_submit_btn");
    private By successMessage = By.xpath("//div[@class='alert alert-success'] ");
    private By backToCategoryListBtn = By.id("desc-category-back");
    private By sornNameAscBtn = By.xpath("//span[contains(text(), 'Имя')]//a[2]");
    private By categoryTable = By.id("table-category");
    private By allCategoriesName = By.xpath("//table[@id = 'table-category']//tbody/tr/td[3]");

    public CategoriesPage(WebDriver driver, WebDriverWait wait) {
        this.driver = driver;
        this.wait = wait;
    }

    public void openCategoriesPage(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(catalogMenuItem));
        moveToElementUsingActions(driver, driver.findElement(catalogMenuItem));

        wait.until(ExpectedConditions.visibilityOfElementLocated(categoriesMenuItem));
        driver.findElement(categoriesMenuItem).click();
    }

    public void waitUntilTitleWillBeVisible(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(title));
    }

    public void clickOnAddCategoryBtn(){
        wait.until(ExpectedConditions.elementToBeClickable(addCategoryBtn));
        driver.findElement(addCategoryBtn).click();
    }

    public void fillCategoryName(String categoryName){
        wait.until(ExpectedConditions.elementToBeClickable(categoryNameInput));
        driver.findElement(categoryNameInput).sendKeys(categoryName);
    }

    public void clickSaveBtn(){
        WebElement saveButton = driver.findElement(saveBtn);
        scrollToElementUsingJavaScript(driver, saveButton);
        saveButton.submit();
    }

    public void waitUntilSuccessMessageWillBeVisible(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(successMessage));
    }

    public void clickBackToCategoryListBtn(){
        WebElement backToCategoryListButton = driver.findElement(backToCategoryListBtn);
        scrollToElementUsingJavaScript(driver, backToCategoryListButton);
        backToCategoryListButton.click();
    }

    public void sortByNameasc(){
        wait.until(ExpectedConditions.elementToBeClickable(sornNameAscBtn));
        driver.findElement(sornNameAscBtn).click();
    }

    public boolean validatesIfCategoryIsAddedIntoTable(String categoryName){
        wait.until(ExpectedConditions.visibilityOfElementLocated(categoryTable));
        List<WebElement> categoriesName = driver.findElements(allCategoriesName);
        for (WebElement name : categoriesName) {
            if (name.getText().equals(categoryName))
                return true;
        }
        return false;
    }
}
